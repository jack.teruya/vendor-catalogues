from django.db import models


class Vendor(models.Model):
    id = models.IntegerField(primary_key=True, editable=False, auto_created=True)
    name = models.CharField(max_length=200, null=False)
    cnpj = models.CharField(max_length=14, null=False, unique=True)
    city = models.CharField(max_length=200)

    def __str__(self):
        return self.nome
