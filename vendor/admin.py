from django.contrib import admin

from vendor.models import Vendor

@admin.register(Vendor)
class VendorAdmin(admin.ModelAdmin):
    list_display = ('name', 'cnpj', 'city')
