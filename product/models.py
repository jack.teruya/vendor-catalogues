from django.db import models


class Product(models.Model):
    id = models.IntegerField(primary_key=True, editable=False, auto_created=True)
    name = models.CharField(max_length=200, null=False)
    code = models.CharField(max_length=15, null=False)
    price = models.DecimalField(max_digits=5, decimal_places=5)

    def __str__(self):
        return self.nome
