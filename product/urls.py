from django.urls import path

from product.views import ProductCreateView, ProductListView

urlpatterns = [
    path('create/', ProductCreateView.as_view(), name='create'),
    path('list/', ProductListView.as_view(), name='list')
]
