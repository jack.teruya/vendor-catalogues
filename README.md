# API Vendor Catalogues

This API register vendor catalogues. 
Contain a CRUD for products catalogues and a CRUD for vendor catalogues. 
A product requires a vendor and can only have one vendor.


# Requirements
 - Python 3.8+ for Django backend

